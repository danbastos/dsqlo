unit FormMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Generics.Collections, Annotations;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    btn1: TButton;
    Button1: TButton;
    procedure btn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);

  end;

  [ORMEntity()]
  TPerson = class(TObject)
  private
    [ORMColumn('name:id,dbNullable:true')]
    FID : Int64;

  public
    property id : Int64 read FID;

  end;

  [ORMEntity()]
  TDependent = class(TObject)
  public
    numero : Integer;
    nome : string;
    t1 : Real;
    //t2 : Real48; // N�o funciona
    t3 : Double;
    t4 : Currency;
    //t5 : Text; // N�o funciona
  end;

  [ORMEntity()]
  TClient = class(TPerson)
  private
    Fvalor : Extended;

  public
    [ORMColumn('name:nasc')]
    nome : string;
    dependent : TDependent;
    dependents : TList<TDependent>;
    property valor : Extended read Fvalor write FValor;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses EntityManager, Metadata;

procedure TForm1.btn1Click(Sender: TObject);
var
  emm : TEntityMetadataManager;
  el : TPair<String,TEntityMetadata>;
begin
  Memo1.Clear;

  emm := TEntityMetadataManager.Create;
  emm.Add(TClient);

  for el in emm.ToArray do
  begin
    Memo1.Lines.Add(el.Value.ToString);
    Memo1.Lines.Add('');
  end;

  emm.Free;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
//  TEntityManager.get.Find<TClient>('');

end;

end.
