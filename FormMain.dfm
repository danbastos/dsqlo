object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 409
  ClientWidth = 1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  DesignSize = (
    1080
    409)
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 8
    Top = 32
    Width = 1066
    Height = 368
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      'FieldMetadata'
      '    name        : string;   QualifiedName'
      '    TypeName    : string;'
      ''
      '    dbName      : string;'
      '    dbTypeName  : string;'
      '    dbSize      : integer;'
      '    dbPrecision : integer;'
      '    dbNullable  : Boolean;'
      '    generator   : boolean;'
      ''
      'RelationMetadata'
      '    Name      : String '#39'FK_'#39'+Owner+'#39'_'#39'+Dest+'#39'_'#39'+Sequencial'
      
        '    TypeName  : String Criar tipo com as op'#231#245'es (HasOne, HasMany' +
        ')'
      '    owner     : String'
      '    dest      : String'
      ''
      ''
      'EntityMetadata'
      '    Name      : string'
      '    Fields    : TFieldMetadataDict'
      '    Relations : TRelationMetadataDict')
    ParentFont = False
    TabOrder = 0
  end
  object btn1: TButton
    Left = 8
    Top = 3
    Width = 75
    Height = 25
    Caption = 'btn1'
    TabOrder = 1
    OnClick = btn1Click
  end
  object Button1: TButton
    Left = 84
    Top = 3
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 2
    OnClick = Button1Click
  end
end
