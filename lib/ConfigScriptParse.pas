unit ConfigScriptParse;

interface

uses System.Types, System.StrUtils, System.Generics.Collections, System.Generics.Defaults;

type
  TConfigList = TDictionary<String, String>;

  TConfigScriptParse = class
  protected
    FConfigSource : string;
    FConfigData : TConfigList;

    procedure parseConfig;

  public
    constructor Create(config : string);
    destructor Destroy; override;

    property Config : TConfigList read FConfigData;

  end;

implementation

{ TConfigScriptParse }

constructor TConfigScriptParse.Create(config: string);
begin
  FConfigSource := config;
  FConfigData := TConfigList.Create;
  parseConfig;
end;

destructor TConfigScriptParse.Destroy;
begin
  FConfigData.Free;
  inherited;
end;

procedure TConfigScriptParse.parseConfig;
var
  parList, valList : TStringDynArray;
  par : string;
begin
  parList := SplitString(FConfigSource, ';');
  for par in parList do
  begin
    valList := SplitString(par, ':');
    FConfigData.AddOrSetValue(valList[0], valList[1]);
  end;
end;

end.
