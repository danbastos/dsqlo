unit EntityManager;

interface

uses
  System.SysUtils, System.Generics.Collections, Metadata;

type
  TEntityManager = class(TInterfacedObject)
  private
    FMetadata: TEntityMetadataManager;

  public
    class function get : TEntityManager;

    constructor Create;
    destructor Destroy; override;

    function Find<T: class>(Criteria : string) : TArray<T>;
    property Metadata : TEntityMetadataManager read FMetadata;
  end;

implementation

var
  emInstance : TEntityManager;

{ TEntityManager }

constructor TEntityManager.Create;
begin
  FMetadata := TEntityMetadataManager.Create;
end;

destructor TEntityManager.Destroy;
begin
  FMetadata.Free;
  inherited;
end;

function TEntityManager.Find<T>(Criteria: string): TArray<T>;
var
  entity : TEntityMetadata;

begin
  metadata.Add(TClass(T));
  if (not metadata.TryGetValue(T.ClassName, entity)) then
    raise Exception.Create('No metadata found by class.');

end;

class function TEntityManager.get: TEntityManager;
begin
  result := emInstance;
end;

Initialization
  emInstance := TEntityManager.Create;

Finalization
  emInstance.Free;

end.
