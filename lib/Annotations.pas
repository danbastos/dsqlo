unit Annotations;

interface

uses System.SysUtils, System.Generics.Collections, System.StrUtils, System.Types, ConfigScriptParse;

type
  TDomainAtribute = class abstract (TCustomAttribute)
  private
    FConfigScriptParse : TConfigScriptParse;
    function getConfig : TConfigList;
    procedure ValidateConfig;

  protected
    function isValidConfig(key, value : string) : Boolean; virtual; abstract;

  public
    constructor Create(config : string = '');
    destructor Destroy; override;
    property Config : TConfigList read getConfig;
  end;

  ORMEntity = class(TDomainAtribute)
  protected
    function isValidConfig(key, value : string) : Boolean; override;
  end;

  ORMColumn = class(TDomainAtribute)
  protected
    function isValidConfig(key, value : string) : Boolean; override;
  end;

implementation

{ TDomainAtribute }

constructor TDomainAtribute.Create(config: string);
begin
  inherited Create;
  FConfigScriptParse := TConfigScriptParse.Create(config);
  ValidateConfig;
end;

destructor TDomainAtribute.Destroy;
begin
  FConfigScriptParse.Free;
  inherited;
end;

function TDomainAtribute.getConfig: TConfigList;
begin
  result := FConfigScriptParse.Config;
end;

procedure TDomainAtribute.ValidateConfig;
var
  el : TPair<string,string>;

begin
  for el in Config.ToArray do
  begin
    if not isValidConfig(el.Key, el.Value) then
      raise Exception.Create('Invalid object configuration. [key:'+el.Key+', value:'+el.Value+']');
  end;
end;

{ ORMEntity }

function ORMEntity.isValidConfig(key, value: string): Boolean;
begin
  result := True;
end;

{ ORMColumn }

function ORMColumn.isValidConfig(key, value: string): Boolean;
begin
  result := True;
end;

end.
