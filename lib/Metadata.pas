unit Metadata;

interface

uses
  System.StrUtils, System.Generics.Collections, System.Generics.Defaults, System.Rtti, System.Variants, System.SysUtils, System.TypInfo;

type
  TEntityMetadata   = class;
  TFieldMetadata    = class;
  TRelationMetadata = class;

  TMObjectDictionary<TKey,TValue> = class(TObjectDictionary<TKey,TValue>);

  TFieldMetadataDict    = TMObjectDictionary<String,TFieldMetadata>;
  TRelationMetadataDict = TMObjectDictionary<String,TRelationMetadata>;

  TFieldMetadata = class(TObject)
  protected
    function makeDBFieldName : string;  virtual;
    function makeDBTypeName  : string;  virtual;
    function makeDBSize      : integer; virtual;
    function makeDBPrecision : integer; virtual;
    function makeDBNullable  : boolean; virtual;
    function makeDBgenerator : boolean; virtual;

  public
    name        : string;
    TypeName    : string;
    location    : string;

    dbName      : string;
    dbTypeName  : string;
    dbSize      : integer;
    dbPrecision : integer;
    dbNullable  : Boolean;
    generator   : boolean;

    constructor Create;
    procedure loadInfo(RttiField : TRttiField);

    function ToString : string; override;
  end;

  TRelationMetadata = class(TObject)
  private
    Fname      : String;
    FTypeName  : String;
    Fowner     : String;
    Fdest      : String;

    procedure Setdest(const Value: String);
    procedure Setname(const Value: String);
    procedure Setowner(const Value: String);
    procedure SetTypeName(const Value: String);

  public
    property name      : String          read Fname     write Setname;
    property TypeName  : String          read FTypeName write SetTypeName;
    property owner     : String          read Fowner    write Setowner;
    property dest      : String          read Fdest     write Setdest;

    constructor Create();
    procedure loadInfo(RttiField : TRttiField);
    destructor Destroy; override;

    function ToString : string; override;
  end;

  TEntityMetadata = class(TObject)
  private
    FName      : string;
    FFields    : TFieldMetadataDict;
    FRelations : TRelationMetadataDict;

    procedure SetName      (const Value: string);
    procedure SetFields    (const Value: TFieldMetadataDict);
    procedure SetRelations (const Value: TRelationMetadataDict);

  public
    property Name      : string                read FName      write SetName;
    property Fields    : TFieldMetadataDict    read FFields    ;
    property Relations : TRelationMetadataDict read FRelations ;

    constructor Create();
    destructor Destroy; override;

    function ToString : string; override;
  end;

  TEntityMetadataManager = class(TObjectDictionary<String,TEntityMetadata>)
  protected
    RttiContext : TRttiContext;

    procedure ApplyAttributes(RttiType : TRttiType; obj: TObject);
    function  CreateEntity(RttiType : TRttiType) : TEntityMetadata;

    procedure CreateFields(Fields : TFieldMetadataDict; RttiType : TRttiType);
    function  CreateField(RttiField : TRttiField) : TFieldMetadata;

    procedure CreateRelations(Relations : TRelationMetadataDict; RttiType : TRttiType);
    function  CreateRelation(RttiField : TRttiField) : TRelationMetadata;

  public
    constructor Create();
    destructor Destroy; override;
    procedure Add(AClass : TClass); overload;
    procedure Add(AQualifiedName : string); overload;
  end;

implementation

uses Annotations;

procedure TEntityMetadataManager.Add(AQualifiedName: string);
var
  em : TEntityMetadata;
  RttiType : TRttiType;
  CustomAttribute : TCustomAttribute;
  CanMap : Boolean;
begin
  RttiType := RttiContext.FindType(AQualifiedName);

  CanMap := false;
  for CustomAttribute in RttiType.GetAttributes do
    if CustomAttribute is ORMEntity then
      CanMap := True;

  if not CanMap then Exit;

  Add(RttiType.BaseType.QualifiedName);

  em := CreateEntity(RttiType);
  Add(em.Name, em);
end;

procedure TEntityMetadataManager.ApplyAttributes(RttiType: TRttiType; obj: TObject);
var
  CustomAttribute : TCustomAttribute;
  ConfigElement : TPair<string, string>;
  LocalRttiType: TRttiType;
  prop: TRTTIProperty;
  value: TValue;
begin
  LocalRttiType := RttiContext.FindType(obj.QualifiedClassName);
  //LocalRttiContext := TRTTIContext.Create;
  for CustomAttribute in RttiType.GetAttributes do
    if CustomAttribute is TDomainAtribute then
    begin
      for ConfigElement in TDomainAtribute(CustomAttribute).Config.ToArray do
      begin
        prop := LocalRttiType.GetProperty(ConfigElement.Key);
        if Assigned(prop) then
        begin
          value := ConfigElement.Value;
          prop.SetValue(obj, value);
        end;
      end;
    end;
end;

constructor TEntityMetadataManager.Create();
begin
  inherited Create;
  RttiContext := TRttiContext.Create;
end;

destructor TEntityMetadataManager.Destroy;
var
  Field : TPair<string, TEntityMetadata>;
begin
  RttiContext.Free;
  for Field in ToArray do
    Field.Value.Free;
  inherited Destroy;
end;

function TEntityMetadataManager.CreateEntity(RttiType : TRttiType) : TEntityMetadata;
begin
  Result := TEntityMetadata.Create;
  Result.Name := RttiType.Name;
  ApplyAttributes(RttiType, Result);

  CreateFields(Result.Fields, RttiType);
  CreateRelations(Result.Relations, RttiType);
end;

procedure TEntityMetadataManager.CreateFields(Fields : TFieldMetadataDict; RttiType : TRttiType);
var
  RttiField : TRttiField;

begin
  for RttiField in RttiType.GetDeclaredFields do
  begin
    if (RttiField.FieldType.IsInstance) then Continue;
    if (Pos('System.Generics.Collections', RttiField.FieldType.QualifiedName) > 0) then Continue;
    if (Pos('System.Generics.Defaults', RttiField.FieldType.QualifiedName) > 0) then Continue;

    Fields.AddOrSetValue(RttiField.Name, CreateField(RttiField));
  end;
end;

function TEntityMetadataManager.CreateRelation(RttiField: TRttiField): TRelationMetadata;
begin
  result := TRelationMetadata.Create;
  Result.loadInfo(RttiField);
end;

procedure TEntityMetadataManager.CreateRelations(Relations: TRelationMetadataDict; RttiType: TRttiType);
var
  RttiField : TRttiField;
  rm : TRelationMetadata;
begin
  for RttiField in RttiType.GetDeclaredFields do
  begin
    if (not RttiField.FieldType.IsInstance) then Continue;
    Add(RttiField.FieldType.QualifiedName);
    Relations.AddOrSetValue(RttiType.ClassName+IntToStr(Relations.Count),  CreateRelation(RttiField));
  end;
end;

function TEntityMetadataManager.CreateField(RttiField : TRttiField): TFieldMetadata;
begin
  Result := TFieldMetadata.Create;
  Result.loadInfo(RttiField);
end;

procedure TEntityMetadataManager.Add(AClass : TClass);
begin
  Add(AClass.QualifiedClassName);
end;

{ TFieldMetadata }

constructor TFieldMetadata.Create;
begin
  inherited Create;
  name        := '';
  TypeName    := '';
  location    := '';

  dbName      := '';
  dbTypeName  := '';
  dbSize      := 0;
  dbPrecision := 0;
  dbNullable  := True;
end;

procedure TFieldMetadata.loadInfo(RttiField: TRttiField);
var
  ca : TCustomAttribute;
  attrVal : string;
begin
  name       := RttiField.Name;
  location   := RttiField.Parent.QualifiedName;
  TypeName   := RttiField.FieldType.QualifiedName;

  dbName      := makeDBFieldName;
  dbTypeName  := makeDBTypeName;
  dbSize      := makeDBSize;
  dbPrecision := makeDBPrecision;
  dbNullable  := makeDBNullable;
  generator   := makeDBgenerator;

  for ca in RttiField.GetAttributes do
  begin
    if ca is ORMColumn then
    begin
      if (ORMColumn(ca).Config.TryGetValue('name'       , attrVal)) then dbName     := attrVal;
      if (ORMColumn(ca).Config.TryGetValue('typename'   , attrVal)) then dbTypeName := attrVal;
    end;
  end;
end;

function TFieldMetadata.makeDBFieldName: string;
begin
  result := Name;
end;

function TFieldMetadata.makeDBgenerator: boolean;
begin
  result := false;
end;

function TFieldMetadata.makeDBNullable: boolean;
begin
  result := True;
end;

function TFieldMetadata.makeDBPrecision: integer;
begin
  result := 0;
//  if TypeName = 'System.string'  then result := 255;
end;

function TFieldMetadata.makeDBSize: integer;
begin
  result := 0;
  if TypeName = 'System.string'  then result := 255;
end;

function TFieldMetadata.makeDBTypeName: string;
begin
  result := 'varchar';
  if TypeName = 'System.Integer'  then result := 'integer';
  if TypeName = 'System.Extended' then result := 'float';
  if TypeName = 'System.Int64'    then result := 'bigint';
end;

function TFieldMetadata.ToString: string;
begin
  Result := location+'.'+name+', TypeName' + ':'+ TypeName;
  if (dbName <> '') then
  begin
    Result := Result +
    ', dbName:'+dbName+
    ', dbTypeName:'+dbTypeName+
    ', dbSize:'+IntToStr(dbSize)+
    ', dbPrecision:'+IntToStr(dbPrecision)+
    ', dbNullable:'+BoolToStr(dbNullable)+
    ', generator:'+BoolToStr(generator);
  end;

end;

{ TEntityMetadata }

constructor TEntityMetadata.Create;
begin
  inherited;
  FFields    := TFieldMetadataDict    .Create;
  FRelations := TRelationMetadataDict .Create;
end;

destructor TEntityMetadata.Destroy;
var
  Field : TPair<string, TFieldMetadata>;
  Relation : TPair<string, TRelationMetadata>;

begin
  for Field in FFields.ToArray do Field.Value.Free;
  for Relation in FRelations.ToArray do Relation.Value.Free;

  FFields    .Free;
  FRelations .Free;
  inherited;
end;

procedure TEntityMetadata.SetFields(const Value: TFieldMetadataDict);
begin
  FFields.Free;
  FFields := Value;
end;

procedure TEntityMetadata.SetName(const Value: string);
begin
  FName := Value;
end;

procedure TEntityMetadata.SetRelations(const Value: TRelationMetadataDict);
begin
  FRelations.Free;
  FRelations := Value;
end;

function TEntityMetadata.ToString: string;
var
  Field : TPair<string, TFieldMetadata>;
  Relation : TPair<string, TRelationMetadata>;
begin
  Result := 'name:'+ Name;
  for Field in Fields.ToArray do Result := Result + sLineBreak + 'Fields => '+Field.Value.ToString;
  for Relation in Relations.ToArray do Result := Result + sLineBreak + 'Relations =>'+Relation.Value.ToString;
end;

{ TRelationMetadata }

constructor TRelationMetadata.Create;
begin
end;

destructor TRelationMetadata.Destroy;
begin
  inherited;
end;

procedure TRelationMetadata.loadInfo(RttiField: TRttiField);

 function isHasManyClass(DestQualifiedName : string) : boolean;
 begin
   result := (Pos('System.Generics.Collections.TList<', DestQualifiedName) > 0) ;
 end;

var
  ca : TCustomAttribute;
  attrVal : string;

begin
  name := 'fk_'+RttiField.Parent.Name;

  TypeName := 'HasOne';
  owner := RttiField.Parent.QualifiedName;
  dest := RttiField.FieldType.QualifiedName;

  if isHasManyClass(dest) then
    TypeName := 'HasMany';
end;

procedure TRelationMetadata.Setdest(const Value: String);
begin
  Fdest := Value;
end;

procedure TRelationMetadata.Setname(const Value: String);
begin
  Fname := Value;
end;

procedure TRelationMetadata.Setowner(const Value: String);
begin
  Fowner := Value;
end;

procedure TRelationMetadata.SetTypeName(const Value: String);
begin
  FTypeName := Value;
end;

function TRelationMetadata.ToString: string;
begin
  result := name+'|'+TypeName+'|'+owner+'|'+dest;
end;

end.
