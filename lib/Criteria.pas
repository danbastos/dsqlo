unit Criteria;

interface

type
  TCriteria = class
  private
    Fcondition: string;
    Fjoin: string;
    Flimit: integer;
    Forder: string;
    Fdistinct: boolean;
    FlazyLoad: boolean;
    Falias: string;
    Fhaving: string;
    Foffset: integer;
    Fgroup: string;
    Fselect: string;
    procedure Setalias(const Value: string);
    procedure Setcondition(const Value: string);
    procedure Setdistinct(const Value: boolean);
    procedure Setgroup(const Value: string);
    procedure Sethaving(const Value: string);
    procedure Setjoin(const Value: string);
    procedure SetlazyLoad(const Value: boolean);
    procedure Setlimit(const Value: integer);
    procedure Setoffset(const Value: integer);
    procedure Setorder(const Value: string);
    procedure Setselect(const Value: string);
  public
    property select     : string read Fselect write Setselect;
    property condition  : string read Fcondition write Setcondition;
    property alias      : string read Falias write Setalias;
    property distinct   : boolean read Fdistinct write Setdistinct;
    property group      : string read Fgroup write Setgroup;
    property having     : string read Fhaving write Sethaving;
    property limit      : integer read Flimit write Setlimit;
    property offset     : integer read Foffset write Setoffset;
    property order      : string read Forder write Setorder;
    property join       : string read Fjoin write Setjoin;
    property lazyLoad   : boolean read FlazyLoad write SetlazyLoad;
//    scopes     :
  end;


implementation

{ TCriteria }

procedure TCriteria.Setalias(const Value: string);
begin
  Falias := Value;
end;

procedure TCriteria.Setcondition(const Value: string);
begin
  Fcondition := Value;
end;

procedure TCriteria.Setdistinct(const Value: boolean);
begin
  Fdistinct := Value;
end;

procedure TCriteria.Setgroup(const Value: string);
begin
  Fgroup := Value;
end;

procedure TCriteria.Sethaving(const Value: string);
begin
  Fhaving := Value;
end;

procedure TCriteria.Setjoin(const Value: string);
begin
  Fjoin := Value;
end;

procedure TCriteria.SetlazyLoad(const Value: boolean);
begin
  FlazyLoad := Value;
end;

procedure TCriteria.Setlimit(const Value: integer);
begin
  Flimit := Value;
end;

procedure TCriteria.Setoffset(const Value: integer);
begin
  Foffset := Value;
end;

procedure TCriteria.Setorder(const Value: string);
begin
  Forder := Value;
end;

procedure TCriteria.Setselect(const Value: string);
begin
  Fselect := Value;
end;

end.
