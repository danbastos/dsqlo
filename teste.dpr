program teste;

uses
  Vcl.Forms,
  FormMain in 'FormMain.pas' {Form1},
  EntityManager in 'lib\EntityManager.pas',
  Metadata in 'lib\Metadata.pas',
  Generators in 'lib\Generators.pas',
  Annotations in 'lib\Annotations.pas',
  Criteria in 'lib\Criteria.pas',
  ConfigScriptParse in 'lib\ConfigScriptParse.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
